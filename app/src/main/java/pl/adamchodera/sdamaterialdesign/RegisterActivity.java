package pl.adamchodera.sdamaterialdesign;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.name_field)
    TextInputLayout nameField;

    @BindView(R.id.email_field)
    TextInputLayout emailField;

    @BindView(R.id.password_field)
    TextInputLayout passwordField;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.register_button)
    protected void attemptRegistration() {
        boolean isNameValid = isFullNameValid();

        if (isNameValid) {
            nameField.setError(null);
        } else {
            nameField.setError(getString(R.string.error_name_is_too_short));
        }

        boolean isEmailValid = isEmailValid();

        if (isEmailValid) {
            emailField.setError(null);
        } else {
            emailField.setError(getString(R.string.error_email_not_valid));
        }

        boolean isPasswordValid = isPasswordValid();
        if (isPasswordValid) {
            passwordField.setError(null);
        } else {
            passwordField.setError(getString(R.string.error_password_is_too_short));
        }

        if (isEmailValid && isPasswordValid) {
            register();
        }
    }

    private boolean isFullNameValid() {
        String name = nameField.getEditText().getText().toString();

        if (name.length() >= 4) {
            return true;

        } else {
            return false;
        }

    }

    private void register() {

    }

    private boolean isEmailValid() {
        String email = emailField.getEditText().getText().toString();

        if (email.contains("@")) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isPasswordValid() {
        String password = passwordField.getEditText().getText().toString();

        if (password.length() >= 4) {
            return true;
        } else {
            return false;
        }
    }


    @OnClick(R.id.login_button)
    protected void goToLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
