package pl.adamchodera.sdamaterialdesign;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.email_field)
    TextInputLayout emailField;

    @BindView(R.id.password_field)
    TextInputLayout passwordField;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.login_button)
    protected void attemptLogin() {
        boolean isEmailValid = isEmailValid();

        if (isEmailValid) {
            emailField.setError(null);
        } else {
            emailField.setError(getString(R.string.error_email_not_valid));
        }

        boolean isPasswordValid = isPasswordValid();
        if (isPasswordValid) {
            passwordField.setError(null);
        } else {
            passwordField.setError(getString(R.string.error_password_is_too_short));
        }

        if (isEmailValid && isPasswordValid) {
            login();
        }
    }

    @OnClick(R.id.register_button)
    protected void goToRegisterScreen() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    private void login() {
        // TODO check with the server
        goToDashboard();
    }

    private void goToDashboard() {
        // TODO implement
    }

    private boolean isEmailValid() {
        String email = emailField.getEditText().getText().toString();

        if (email.contains("@")) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isPasswordValid() {
        String password = passwordField.getEditText().getText().toString();

        if (password.length() >= 4) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
